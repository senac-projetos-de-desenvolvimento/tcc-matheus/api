
exports.up = function (knex) {
    return knex.schema.createTable('pluviometers', table => {
        table.increments('id').primary()
        table.string('quantityMM').notNull()
        table.string('doneAt')
        table.integer('userId').references('id').inTable('users').notNull()
    })
};

exports.down = function (knex) {
    return knex.schema.dropTableIfExists('pluviometers')
};
