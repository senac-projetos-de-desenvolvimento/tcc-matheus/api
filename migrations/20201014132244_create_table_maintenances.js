exports.up = function (knex) {
    return knex.schema.createTable('maintenances', table => {
        table.increments('id').primary()
        table.string('implement').notNull()
        table.string('desc').notNull()
        table.string('doneAt')
        table.integer('userId').references('id').inTable('users').notNull()
    })
}

exports.down = function (knex) {
    return knex.schema.dropTableIfExists('maintenances')
}
