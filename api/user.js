const bcrypt = require('bcrypt-nodejs')

module.exports = app => {
    const obterHash = (password, callback) => {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(password, salt, null, (err, hash) => callback(hash))
        })
    }

    const save = (req, res) => {
        obterHash(req.body.password, hash => {
            const password = hash

            console.log(req.body)
            app.db('users')
                .insert({
                    name: req.body.name,
                    email: req.body.email.toLowerCase(),
                    password,
                    farm: req.body.farm
                })
                .then(_ => res.status(204).send())
                .catch(err => res.status(400).json(err))
        })
    }

    const getUser = (req, res) => {
        app.db('users')
            .where({ id: req.user.id })
            .then(user => { 
                console.log(user)
                res.json(user) })
            .catch(err => res.status(500).json(err))
    }

    return { save, getUser }
}