const moment = require('moment')

module.exports = app => {
    
    const getDeliveries = (req, res) => {
        console.log(req.user)
        app.db('deliveries')
            .where({ userId: req.user.id })
            .then(deliveries => {res.json(deliveries)})
            .catch(err => res.status(500).json(err))
    }

    const save = (req, res) => {
        if (!req.body.desc.trim()) {
            return res.status(400).send('Compania é um campo obrigatório')
        }

        req.body.userId = req.user.id

        app.db('deliveries')
            .insert(req.body)
            .then(_ => res.status(204).send())
            .catch(err => res.status(400).json(err))
    }

    const remove = (req, res) => {
        app.db('deliveries')
            .where({ id: req.params.id, userId: req.user.id })
            .del()
            .then(rowsDeleted => {
                if (rowsDeleted > 0) {
                    res.status(204).send()
                } else {
                    const msg = `Não foi encontrada entrega com id ${req.params.id}`
                    res.status(400).send(msg)
                }
            })
    }

    const updateDeliverieDoneAt = (req, res, doneAt) => {
        app.db('deliveries')
            .where({ id: req.params.id, userId: req.user.id })
            .update({ doneAt })
            .then(_ => res.status(204).send())
            .cath(err => res.status(400).json(err))
    }

    const toggleDeliverie = (req, res) => {
        app.db('deliveries')
            .where({ id: req.params.id, userId: req.user.id })
            .first()
            .then(deliverie => {
                if (!deliveries) {
                    const msg = `Entrega com id ${req.params.id} não encontrado.`
                    return res.status(400).send(msg)
                }

                const doneAt = deliverie.doneAt ? null : new Date()
                updateDeliverieDoneAt(req, res, doneAt)
            })
            .cath(err => res.status(400).json(err))
    }

    return { getDeliveries, save, remove, toggleDeliverie}
}